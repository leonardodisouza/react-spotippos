const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin('styles/style-[hash].css');
const exclude = [/node_modules/];
const path = require('path');

module.exports = {
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel', exclude },
      {
        test: /\.styl$/,
        loader: extractCSS.extract(['css-loader', 'stylus-loader?paths=app/styles']),
        exclude,
      },
      { test: /\.json$/, loader: 'json' },
      { test: /\.html$/, loader: 'html', exclude },
      { test: /\.png$/, loader: 'url-loader', exclude },
      { test: /\.svg$/, loader: 'svg-url-loader', exclude },
    ],
  },
  resolve: {
    root: [
      path.resolve('./'),
    ],
  },
  node: {
    fs: 'empty',
  },
  plugins: [
    extractCSS,
  ],
  externals: {
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
};
