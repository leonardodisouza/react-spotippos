const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const indexHTML = new HtmlWebpackPlugin({ template: './index.html' });
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin('styles/style-[hash].css');
const optimize = new webpack.optimize.CommonsChunkPlugin('main', 'scripts/main-[hash].js');
const exclude = [/node_modules/];

module.exports = {
  context: __dirname,
  entry: './app/main.js',
  output: {
    path: `${__dirname}/dist`,
    filename: 'scripts/main.js',
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel', exclude },
      {
        test: /\.styl$/,
        loader: extractCSS.extract(['css-loader', 'stylus-loader?paths=app/styles']),
        exclude,
      },
      { test: /\.json$/, loader: 'json' },
      { test: /\.html$/, loader: 'html', exclude },
      { test: /\.png$/, loader: 'url-loader', exclude },
      { test: /\.svg$/, loader: 'svg-url-loader', exclude },
    ],
  },
  plugins: [
    extractCSS,
    indexHTML,
    optimize,
  ],
};
