import propertiesReducer, { propertyShape } from 'app/reducers/properties_reducer';
import { expect } from 'chai';
import _ from 'lodash';
import Rx from 'rx';

describe('#propertiesReducer', () => {
  it('accepts the LOAD_PROPERTIES action type and returns a list of properties', (done) => {
    const action = {
      type: 'LOAD_PROPERTIES',
      payload: {
        page: 1,
      },
    };

    const result = propertiesReducer(action, Rx.Observable.of({}));
    result.subscribe((state) => {
      expect(state.data.page).to.be.eql(1);
      expect(state.data.items.length).to.be.eql(9);
      done();
    });
  });

  it('formats the properties based on propertyShape type', (done) => {
    const action = {
      type: 'LOAD_PROPERTIES',
      payload: {
        page: 1,
      },
    };

    const result = propertiesReducer(action, Rx.Observable.of({}));
    const shapeKeys = _.keys(propertyShape);

    result.subscribe((state) => {
      state.data.items.forEach((item) => {
        expect(item).to.contain.all.keys(shapeKeys);
        done();
      });
    });
  });

  it('only changes the data and properties key on the given state', (done) => {
    const action = {
      type: 'LOAD_PROPERTIES',
      payload: {
        page: 1,
      },
    };

    const state = Rx.Observable.of({ foo: 'bar' });

    const result = propertiesReducer(action, state);
    result.subscribe((newState) => {
      expect(_.omit(newState, ['data', 'properties'])).to.be.eql({ foo: 'bar' });
      done();
    });
  });

  it('returns the same given state when the given action is not a expected type', () => {
    const action = {
      type: 'ANOTHER_TYPE',
    };
    const state = Rx.Observable.of({});
    expect(propertiesReducer(action, state)).to.be.equal(state);
  });
});
