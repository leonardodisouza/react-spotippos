import propertiesFilterReducer from 'app/reducers/properties_filter_reducer';
import { expect } from 'chai';
import _ from 'lodash';
import { Observable } from 'rx';

describe('#propertiesFilterReducer', () => {
  it('accepts the FILTER_PROPERTIES action type and returns a list of properties', (done) => {
    const properties = [
      { id: 3 },
      { id: 40 },
    ];

    const state = Observable.of({ data: { items: properties } });

    const action = {
      type: 'FILTER_PROPERTIES',
      payload: {
        id: 3,
      },
    };

    const result = propertiesFilterReducer(action, state);
    result.subscribe((newState) => {
      expect(newState.properties).to.be.eql([{ id: 3 }]);
      done();
    });
  });

  it('only changes the properties key on the given state', (done) => {
    const properties = [
      { id: 3 },
      { id: 40 },
    ];

    const state = { data: { items: properties } };

    const action = {
      type: 'FILTER_PROPERTIES',
      payload: {
        id: 3,
      },
    };

    const result = propertiesFilterReducer(action, Observable.of(state));
    result.subscribe((newState) => {
      expect(_.omit(newState, 'properties')).to.be.eql(state);
      done();
    });
  });

  it('returns the same given state when the given action is not a expected type', () => {
    const action = {
      type: 'ANOTHER_TYPE',
    };
    const state = {};
    expect(propertiesFilterReducer(action, state)).to.be.equal(state);
  });
});
