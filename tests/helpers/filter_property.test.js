import filterProperty from 'app/helpers/filter_property';
import { expect } from 'chai';

describe('#filter_property', () => {
  describe('filters the given properties based on the given params', ()  => {
    const properties = [
      {
        id: 1,
        beds: 3,
        baths: 2,
        squareMeters: 90,
        price: 100,
      },
      {
        id: 2,
        beds: 1,
        baths: 1,
        squareMeters: 30,
        price: 50,
      },
      {
        id: 3,
        beds: 1,
        baths: 2,
        squareMeters: 30,
        price: 200,
      },
    ];

    function filter(params) {
      return properties.filter((property) => filterProperty(params, property));
    }

    it('filter by id', () => {
      expect(filter({ id: 1 })).to.be.eql([properties[0]]);
      expect(filter({ id: 2 })).to.be.eql([properties[1]]);
    });

    it('filter by beds', () => {
      expect(filter({ beds: 1 })).to.be.eql([properties[1], properties[2]]);
      expect(filter({ beds: 3 })).to.be.eql([properties[0]]);
    });

    it('filter by baths', () => {
      expect(filter({ baths: 1 })).to.be.eql([properties[1]]);
      expect(filter({ baths: 2 })).to.be.eql([properties[0], properties[2]]);
    });

    it('filter by squareMeters', () => {
      expect(filter({ squareMeters: 10 })).to.be.eql([]);
      expect(filter({ squareMeters: 30 })).to.be.eql([properties[1], properties[2]]);
      expect(filter({ squareMeters: 90 })).to.be.eql([properties[0]]);
    });

    it('filter by price with maxPrice or minPrice', () => {
      expect(filter({ minPrice: 51 })).to.be.eql([properties[0], properties[2]]);
      expect(filter({ maxPrice: 50 })).to.be.eql([properties[1]]);
      expect(filter({ minPrice: 0, maxPrice: 49 })).to.be.eql([]);
    });

    it('filter by more than one parameter', () => {
      expect(filter({ minPrice: 51, baths: 2 })).to.be.eql([properties[0], properties[2]]);
      expect(filter({ minPrice: 50, beds: 1 })).to.be.eql([properties[1], properties[2]]);
      expect(filter({ maxPrice: 100, minPrice: 50, beds: 1 })).to.be.eql([properties[1]]);
    });
  });
});
