import React from 'react';

const SpotCard = ({ thumb, children, footer, className }) => {
  const makeThumb = () => {
    if (!thumb) {
      return '';
    }

    return (
      <div className="spot-card__thumb col-xs-12 col-sm-12 col-lg-5">
        {thumb}
      </div>
    );
  };

  return (
    <section className={`spot-card row middle-xs ${className}`}>
      {makeThumb()}
      <div className="spot-card__content col-xs-12 col-sm-12 col-lg-7">
        <div className="spot-card__content-text">
          {children}
        </div>
        <footer className="spot-card__footer">
          {footer}
        </footer>
      </div>
    </section>
  );
};

SpotCard.propTypes = {
  thumb: React.PropTypes.node,
  children: React.PropTypes.node.isRequired,
  footer: React.PropTypes.node,
  className: React.PropTypes.string,
};

export default SpotCard;
