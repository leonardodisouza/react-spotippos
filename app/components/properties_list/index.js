import React, { Component } from 'react';
// eslint-disable-next-line
import SpotProperty from '../property';

class SpotPropertiesList extends Component {
  constructor(props) {
    super(props);
    this.store = props.store;

    this.state = {
      properties: this.store.getState().properties || [],
    };
  }

  componentWillMount() {
    this.subscription = this.store.subscribe(({ properties }) => {
      this.setState({ properties });
    });
  }

  componentWillUnmount() {
    this.subscription.dispose();
  }

  render() {
    const { properties } = this.state;

    return (
      <div className="spot-property__list">
        {properties.map((property) => <SpotProperty key={property.id} property={property} />)}
      </div>
    );
  }
}

SpotPropertiesList.propTypes = {
  store: React.PropTypes.object.isRequired,
};

export default SpotPropertiesList;
