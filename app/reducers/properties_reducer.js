import _ from 'lodash';
import 'whatwg-fetch';
import queryString from 'query-string';

export const propertyShape = {
  thumb: '',
  title: '',
  id: '',
  description: '',
  beds: '',
  baths: '',
  squareMeters: '',
  price: '',
};

const formatProperty = _.unary(_.partial(_.defaults, _, propertyShape));

export default function propertiesReducer({ type, payload }, stateObservable) {
  switch (type) {
    case 'LOAD_PROPERTIES': {
      const query = queryString.stringify({
        bx: payload.page * 50,
        by: payload.page * 30,
        ax: 1,
        ay: 1,
      });

      const url = `http://spotippos.vivareal.com/properties?${query}`;

      return stateObservable
        .flatMap((state) => (
          fetch(url)
          .then(response => response.json())
          .then((data) => {
            const properties = data.properties.map(formatProperty);
            return _.assign({}, state, {
              data: {
                items: properties,
                page: payload.page,
              },
              properties,
            });
          })
      ));
    }

    default:
      return stateObservable;
  }
}
