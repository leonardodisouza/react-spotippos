import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import storeFactory from './store';
import propertiesReducer from './reducers/properties_reducer';
import propertiesFilterReducer from './reducers/properties_filter_reducer';

const appStore = storeFactory();
appStore.apply(propertiesReducer);
appStore.apply(propertiesFilterReducer);

ReactDOM.render(<App store={appStore} />, document.querySelector('#app'));
