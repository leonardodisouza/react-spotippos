import React, { Component } from 'react';
// eslint-disable-next-line
import style from './styles/app.styl';
import SpotPropertiesList from './components/properties_list';
import SpotAppBar from './components/app_bar';
import SpotSidenav from './components/sidenav';
import SpotFilter from './components/filter';
import SpotLoader from './components/loader';
import SpotInfiniteScroll from './components/infinite_scroll';

class App extends Component {
  constructor(props) {
    super(props);
    this.store = props.store;
    this.state = { page: 1, loading: true };
    this.toggleSideBar = this.toggleSideBar.bind(this);
    this.loadMore = this.loadMore.bind(this);
    this.loadProperties = this.loadProperties.bind(this);
  }

  componentWillMount() {
    this.subscription = this.store.subscribe(({ data: { page } }) => {
      this.setState({ page, loading: false });
    });

    this.loadProperties();
  }

  componentWillUnmount() {
    this.subscription.dispose();
  }

  toggleSideBar() {
    this.refs.sideNav.toggle();
  }

  loadMore() {
    this.setState({ loading: true, page: this.state.page + 1 }, this.loadProperties);
  }

  loadProperties() {
    this.store.dispatch({ type: 'LOAD_PROPERTIES', payload: { page: this.state.page } });
  }

  render() {
    const store = this.props.store;
    const loader = () => {
      if (this.state.loading) {
        return <SpotLoader />;
      }

      return '';
    };

    return (
      <section className="row page-wrapper">
        <SpotSidenav ref="sideNav" className="col-sm" />
        <div className="col-sm">
          <SpotAppBar onClick={this.toggleSideBar} />
          <div className="row wrapper">
            <div className="col-sm-3 col-xs-12 block-content--shadow">
              <SpotFilter store={store} />
            </div>
            <div className="col-sm col-xs-12 block-content--shadow list-items">
              <SpotPropertiesList store={store} />
              {loader()}
              <SpotInfiniteScroll onVisible={this.loadMore} />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

App.propTypes = {
  store: React.PropTypes.object.isRequired,
};

export default App;
